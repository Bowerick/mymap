package com.example.robo.mymap;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;

/**
 * Created by Robert on 19.6.2016.
 */
public class SplashScreenActivity extends Activity {

    private EditText _editText;
    private static final String TAG="SplashScreenActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        init();
        new LoadData(this).execute();
    }

    private void init() {
        _editText = (EditText) findViewById(R.id.editText1);
    }

    private class LoadData extends AsyncTask<Void, String, DBHelper> {
        private Context mContext;
        private DBHelper myDB;

        public LoadData(Context context) {
            mContext = context;
        }
        @Override
        protected DBHelper doInBackground(Void... params) {
            //skopirovanie DB pri prvom spusteni
            Log.i(TAG, "new DBHelper started");
            publishProgress("Init app.");
            myDB = new DBHelper(mContext);
            return myDB;
        }

        @Override
        protected void onProgressUpdate(String... progress) {
            _editText.setText(""+progress[0]);
        }

        @Override
        protected void onPostExecute(DBHelper result) {
            if(result != null) {
                Log.i(TAG, "DBHelper finished");
            }
            Intent mainClass = new Intent(SplashScreenActivity.this, MainActivity.class);
            startActivity(mainClass);
            finish();
        }
    }
}
