package com.example.robo.mymap;

/**
 * Created by Robert on 13.7.2016.
 */
public class WaysNodes {
    private String _name;
    private int _id;
    private int _position;
    private float _latitude;
    private float _longitude;
    private int _type;

    public void setName(String name) {
        this._name = name;
    }

    public double getId() {
        return _id;
    }

    @Override
    public String toString() {
        return "WaysNodes [name=" + _name + ", id=" + _id + ", position=" + _position
                + ", latitude=" + _latitude + ", longitude=" + _longitude + _type + "] ";
    }

    public void setType(int type) {
        this._type = type;
    }

    public int getType() {
        return _type;
    }

    public void setId(int id) {
        this._id = id;
    }

    public void setPosition(int position) {
        this._position = position;
    }

    public float getLatitude() {
        return _latitude;
    }

    public void setLatitude(float latitude) {
        this._latitude = latitude;
    }

    public float getLongitude() {
        return _longitude;
    }

    public void setLongitude(float longitude) {
        this._longitude = longitude;
    }
}


