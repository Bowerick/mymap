package com.example.robo.mymap;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.LinkedList;

/**
 * Created by Robert on 20.6.2016.
 */
public class DBHelper extends SQLiteOpenHelper {

    private static final String TAG = "DBHelper";
    private static final int DATABASE_VERSION = 1;
    private static final String DB_NAME= "MyApplicationDB.db";
    private static String DB_PATH;
    private final Context mContext;



    public DBHelper(Context context) {
        super(context, DB_NAME, null, DATABASE_VERSION);
        this.mContext = context;
        if(android.os.Build.VERSION.SDK_INT >= 17) {
            DB_PATH = context.getApplicationInfo().dataDir + "/databases/";
        } else {
            DB_PATH = "/data/data/" + context.getPackageName() + "/databases/";
        }
        try {
            createDatabase();
        } catch(IOException e) {
            Log.e("DBHelper", e.toString(), e);
        }
    }

    private void createDatabase() throws IOException {
        boolean dbExist = false;
        try {
            dbExist = checkDatabase();
        } catch(Exception e) {
            e.printStackTrace();
            Log.e(TAG, e.toString());
        }

        if(dbExist) {
            Log.i(TAG, "DB exists");
        } else {
            this.getReadableDatabase();
            try {
                copyDatabase();
            } catch(IOException e) {
                e.printStackTrace();
                Log.e(TAG,e.toString());
            }
        }
    }

    private void copyDatabase() throws IOException {
        InputStream myInput = mContext.getAssets().open(DB_NAME);
        String outFileName = DB_PATH + DB_NAME;
        OutputStream myOutput = new FileOutputStream(outFileName,false);
        byte[] buffer = new byte[1024];
        int length;
        while((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }
        myOutput.flush();
        myOutput.close();
        myInput.close();
    }

    private boolean checkDatabase() {
        SQLiteDatabase checkDB = null;
        try {
            String myPath = DB_PATH + DB_NAME;
            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
        } catch(SQLiteException e) {
            e.printStackTrace();
            Log.e(TAG, e.toString());
        }
        if(checkDB != null) {
            checkDB.close();
        }
        return checkDB != null;
    }

    public LinkedList<Nodes> getNodes(double lat,double lon)
    {
        LinkedList<Nodes> nodes = new LinkedList<>();

        double pom=((6371.0*Math.cos(Math.toRadians(lat)))*2*Math.PI)/360.0;
        double pom2=(6371.0*2*Math.PI)/360.0;
        String query = "SELECT * FROM nodes_tags " +
                "where lat<"+(lat+(((30.0/pom2))))+
                " and lat>"+(lat-(((30.0/pom2))))+
                " and lon>"+(lon-((30.0/pom)))+
                " and lon<"+(lon+((30.0/pom)));

        Cursor cursor=null;
        SQLiteDatabase db = this.getReadableDatabase();
        try
        {
            cursor = db.rawQuery(query, null);

        }
        catch(Exception e)
        {
            Log.e(TAG,e.toString());
        }

        Nodes node;
        if (cursor != null) {
            if (cursor.moveToFirst())
            {
                do
                {
                    try
                    {
                        node=new Nodes();
                        node.setLat(cursor.getFloat(0));
                        node.setLon(cursor.getFloat(1));
                        node.setName(cursor.getString(2));
                        node.setEle(Double.parseDouble(cursor.getString(3)));
                        node.setType(cursor.getString(4));
                        nodes.add(node);
                    }
                    catch(Exception e)
                    {
                        Log.i(TAG,""+e.toString());
                    }
                }while (cursor.moveToNext());

                cursor.close();
            }
        }
        db.close();
        return nodes;
    }


    public LinkedList<WaysNodes> getWaysNodes(double lat,double lon, int p)
    {
        LinkedList<WaysNodes> cesty = new LinkedList<>();

        double pom=((6371.0*Math.cos(Math.toRadians(lat)))*2*Math.PI)/360.0;//72.9504356031 pri 49 degress
        double pom2=(6371.0*2*Math.PI)/360.0;//111.194926645
        String query = "select Ways_nodes.way_id,Ways_nodes.way_pos,lat,lon,Ways_tags.tag from Ways_nodes"
                +" join Ways_tags on(Ways_tags.id=Ways_nodes.way_id)"
                + "where lat<"+(lat+ (10.0/pom2) )//(lat+(((30.0/pom2))))
                +" and lat>"+  (lat- (10.0/pom2) )
                +" and lon>"+  (lon- (10.0/pom) )
                +" and lon<"+  (lon+ (10.0/pom) )
                //+" and way_pos%50 = 0 "
                +" order by Ways_nodes.way_id,way_pos";
        Log.i("LAT new", (lat- (5.0/pom2) )+"-"+(lat+ (5.0/pom2) ));
        Log.i("LON new", (lon- (5.0/pom2) )+"-"+(lon+ (5.0/pom2) ));
        Log.i("LAT old", (lat-(((8.0/pom2))))+"-"+(lat+(((8.0/pom2)))));
        Log.i("LON old", (lon-((8.0/pom)))+"-"+(lon-((8.0/pom))));
        Cursor cursor=null;
        SQLiteDatabase db = this.getReadableDatabase();
        try
        {
            cursor = db.rawQuery(query, null);
        }
        catch(Exception e)
        {
            Log.e(TAG,e.toString());
        }
        WaysNodes waysNodes;
        if (cursor != null) {
            try
            {
                if (cursor.moveToFirst())
                {
                    do {
                            waysNodes=new WaysNodes();
                            waysNodes.setId(cursor.getInt(0));//1
                            waysNodes.setPosition(cursor.getInt(1));//2
                            waysNodes.setLatitude(cursor.getFloat(2));//3
                            waysNodes.setLongitude(cursor.getFloat(3));//4
                            waysNodes.setType(cursor.getInt(4));//5
                            cesty.add(waysNodes);

                    } while (cursor.moveToNext());

                    cursor.close();
                }
            } catch(Exception e) {
                Log.e(TAG, e.toString());
            }
        }
        db.close();
        return cesty;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
