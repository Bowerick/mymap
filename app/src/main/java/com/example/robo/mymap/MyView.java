package com.example.robo.mymap;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.location.Location;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;

import java.util.LinkedList;

/**
 * Created by Robert on 30.6.2016.
 */
public class MyView extends View {

    private static final double EARTH_RADIUS = 6371d;
    private LinkedList<WaysNodes> ways;
    private LinkedList<Nodes> nodes;
    private float width;
    private float halfWidth;
    private float height;

    private final float MAX_DISTANCE=10.0f;
    private int offset;

    private Paint compassPaint;
    private Paint peakPaint;
    private Paint cityPaint;
    private Paint cityNamePaint;
    private Paint waterPaint;
    private Paint waterNamePaint;
    private Paint reservoirPaint;
    private Paint yellowIndustrialPaint;
    private Paint blueRoutePaint;//1
    private Paint greenRoutePaint;//2
    private Paint yellowRoutePaint;//3
    private Paint redRoutePaint;//4

    private Path pathReservoirFilled;

    private Paint paint;
    private Path compassPath;
    private Path osmPath;

    private boolean pathReservoirFirst;
    private boolean[] colors;
    private Paint[] paints;
    private String nthColor;
    private int count;

    private boolean peak;
    private boolean city;
    private boolean tourist;
    private boolean water;

    private Context mContext;

    private WaysNodes wayNodes;
    private WaysNodes wayNodesNext;
    private Nodes node;

    private float minLat;
    private float maxLat;
    private float minLon;
    private float maxLon;

    private int elevationProgress;
    private int distanceProgress;

    private Location currentLocation;

    public MyView(Context context) {
        super(context);
        mContext = context;
        init();
    }

    private void init() {
        peak = true;
        city = true;
        tourist = true;
        water = true;
        colors = new boolean[8];
        paints = new Paint[8];
        pathReservoirFirst = true;

        compassPaint = new Paint();
        compassPaint.setColor(getResources().getColor(R.color.red));
        compassPaint.setStyle(Paint.Style.STROKE);
        compassPaint.setTextAlign(Paint.Align.CENTER);
        compassPaint.setFlags(Paint.ANTI_ALIAS_FLAG);

        peakPaint = new Paint();
        peakPaint.setColor(getResources().getColor(R.color.brown));
        peakPaint.setTextAlign(Paint.Align.CENTER);

        cityPaint = new Paint();
        cityPaint.setColor(getResources().getColor(R.color.black));
        cityPaint.setTextAlign(Paint.Align.CENTER);
        cityPaint.setAlpha(50);

        cityNamePaint = new Paint();
        cityNamePaint.setColor(getResources().getColor(R.color.black));
        cityNamePaint.setTextAlign(Paint.Align.CENTER);

        waterPaint = new Paint();
        waterPaint.setColor(getResources().getColor(R.color.blueWater));
        waterPaint.setAlpha(50);
        waterPaint.setTextAlign(Paint.Align.CENTER);
        //waterPaint.setStrokeWidth(3);

        waterNamePaint = new Paint();
        waterNamePaint.setColor(getResources().getColor(R.color.blueWaterName));
        waterNamePaint.setTextAlign(Paint.Align.CENTER);

        reservoirPaint = new Paint();
        reservoirPaint.setColor(getResources().getColor(R.color.blueWater));
        reservoirPaint.setAlpha(50);//200
        reservoirPaint.setStyle(Paint.Style.FILL);
        //waterPaint.setStrokeWidth(1);

        paint = new Paint();
        pathReservoirFilled = new Path();

        blueRoutePaint = new Paint();
        blueRoutePaint.setColor(getResources().getColor(R.color.blueRoute));
        blueRoutePaint.setFlags(Paint.ANTI_ALIAS_FLAG);

        yellowRoutePaint = new Paint();
        yellowRoutePaint.setColor(getResources().getColor(R.color.yellow));
        yellowRoutePaint.setFlags(Paint.ANTI_ALIAS_FLAG);

        yellowIndustrialPaint = new Paint();
        yellowIndustrialPaint.setColor(getResources().getColor(R.color.yellow));
        yellowIndustrialPaint.setAlpha(25);

        greenRoutePaint = new Paint();
        greenRoutePaint.setColor(getResources().getColor(R.color.green));
        //greenRoutePaint.setFlags(Paint.ANTI_ALIAS_FLAG);

        redRoutePaint = new Paint();
        redRoutePaint.setColor(getResources().getColor(R.color.red));
        redRoutePaint.setFlags(Paint.ANTI_ALIAS_FLAG);


        paints[0]= waterPaint;
        paints[1]=blueRoutePaint;
        paints[2]=greenRoutePaint;
        paints[3]=redRoutePaint;
        paints[4]=yellowRoutePaint;
        paints[5]=yellowIndustrialPaint;
        paints[6]=cityPaint;
        paints[7]=reservoirPaint;

        compassPath = new Path();
        osmPath = new Path();
        nodes = new LinkedList<>();
        ways = new LinkedList<>();

        width = getWidth();
        halfWidth = width/2;
        height = width;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        width = getWidth();
        halfWidth = width/2;
        height = getHeight();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = width;
        setMeasuredDimension(width, height);
    }

    /**
     * @param canvas
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        count=0;
        drawNodes(canvas);
        drawWays(canvas);
        drawOSM(canvas);

        canvas.save();
    }

    private void drawOSM(Canvas canvas) {
        osmPath.reset();
        osmPath.moveTo(halfWidth ,height-10 );
        osmPath.lineTo(width ,height-10 );
        canvas.drawTextOnPath(getResources().getString(R.string.Osm), osmPath, 0, 0, cityNamePaint);
    }

    private void drawWays(Canvas canvas) {
        int i = -1;
        for(WaysNodes w: ways) {
            i++;
            if(i >= (ways.size() +-offset))
                break;
            wayNodes = w;
            if(wayNodes.getType()>=0 && wayNodes.getType()<=1 && !isCity())
                continue;
            if(wayNodes.getType()>=2 && wayNodes.getType()<=6 && !isWater())
                continue;
            if(wayNodes.getType()>6 && !isTourist())
                continue;
            wayNodesNext = ways.get(i+offset);
            for(int j=0;j<colors.length;j++)
                colors[j]=false;
            switch (wayNodes.getType()) {
                case 0:
                    paint = yellowRoutePaint;//industrial
                    colors[5]=true;
                    break;
                case 1:
                    paint = cityPaint; //residential
                    colors[6]=true;
                    break;
                case 2:
                    paint = waterPaint;
                    waterPaint.setStrokeWidth(4);
                    colors[0]=true;
                    break;
                case 3:
                    paint = waterPaint;
                    waterPaint.setStrokeWidth(2);
                    colors[0]=true;
                    break;
                case 4:
                    paint = waterPaint;
                    waterPaint.setStrokeWidth(1);
                    colors[0]=true;
                    break;
                case 5:
                    paint = waterPaint;
                    waterPaint.setStrokeWidth(1);
                    colors[0]=true;
                    break;
                case 6:
                    paint = reservoirPaint;
                    colors[7]=true;
                    break;
                default:
                    pickColor(wayNodes.getType());
                    break;
            }
            if(!colors[0] && !colors[1] && !colors[2] && !colors[3] && !colors[4] && !colors[5] && !colors[6] && !colors[7]) {
                paint = peakPaint;
            }
            else {
                while(!colors[count%8]) {
                    count++;
                }
                paint = paints[count%8];
            }
            count++;

            if (wayNodes.getId() == wayNodesNext.getId()) {
                if(wayNodes.getType()==0 || wayNodes.getType()==1 || wayNodes.getType()==6) {
                    //first point of line
                    if(pathReservoirFirst) {
                        pathReservoirFilled.moveTo((height * (wayNodes.getLongitude() - minLon) / (maxLon - minLon)),
                                (width * (maxLat - wayNodes.getLatitude()) / (maxLat - minLat)));
                        pathReservoirFirst = false;
                    }
                    if(wayNodesNext.getType()==0 || wayNodesNext.getType()==1 || wayNodesNext.getType()==6) {
                        pathReservoirFilled.lineTo((height * (wayNodesNext.getLongitude() - minLon) / (maxLon - minLon)),
                                (width * (maxLat - wayNodesNext.getLatitude()) / (maxLat - minLat)));
                    }
                } else {
                    canvas.drawLine((height * (wayNodes.getLongitude() - minLon) / (maxLon - minLon)),
                            (width * (maxLat - wayNodes.getLatitude()) / (maxLat - minLat)),
                            (height * (wayNodesNext.getLongitude() - minLon) / (maxLon - minLon)),
                            (width * (maxLat - wayNodesNext.getLatitude()) / (maxLat - minLat)),
                            paint);
                }
            } else {
                //todo bolean spravit global
                boolean last = true;
                //CONTINUOUS LINE
                //if offset skip last point of line => draw line until the last point of line
                for (int j = i + offset - 1; j > i; j--) {
                    wayNodesNext = ways.get(j);
                    if (wayNodes.getId() == wayNodesNext.getId()) {

                        if(wayNodes.getType()==0 || wayNodes.getType()==1 || wayNodes.getType()==6) {
                                pathReservoirFilled.lineTo((height * (wayNodesNext.getLongitude() - minLon) / (maxLon - minLon)),
                                        (width * (maxLat - wayNodesNext.getLatitude()) / (maxLat - minLat)));

                        } else {

                            canvas.drawLine((width * (wayNodes.getLongitude() - minLon) / (maxLon - minLon)),
                                    (height * (maxLat - wayNodes.getLatitude()) / (maxLat - minLat)),
                                    (width * (wayNodesNext.getLongitude() - minLon) / (maxLon - minLon)),
                                    (height * (maxLat - wayNodesNext.getLatitude()) / (maxLat - minLat)),
                                    paint);
                        }

                        i = j + 1 - offset;
                        last = false;
                        break;

                    }
                }
                if(wayNodes.getType()==0 || wayNodes.getType()==1 || wayNodes.getType()==6) {
                    canvas.drawPath(pathReservoirFilled, paint);
                    pathReservoirFirst = true;
                    pathReservoirFilled.reset();
                }
                if (last) {
                    //if line end on correct last position, offset skip first position of line =>
                    //start drawing next line from first position => continue on +1 index
                    i = i + 1 - offset;
                }
            }

        }
    }

    private void pickColor(int type) {
        nthColor = ""+type;

        //49 = ASCI CODE for 1
        if(nthColor.charAt(2)==49)
            colors[1]=true;
        if(nthColor.charAt(3)==49)
            colors[2]=true;
        if(nthColor.charAt(4)==49)
            colors[3]=true;
        if(nthColor.charAt(5)==49)
            colors[4]=true;
    }

    private void drawNodes(Canvas canvas) {
        //todo foreach....
        for(int i=0;i<nodes.size();i++) {
            node=nodes.get(i);
            if((node.getType().compareTo("peak")==0 && !isPeak())
                    ||(node.getType().compareTo("city")==0 && !isCity())
                    ||(node.getType().compareTo("water")==0 && !isWater())
                    ||(node.getLat() < minLat)
                    ||(node.getLat() > maxLat)
                    ||(node.getLon() < minLon)
                    ||(node.getLon() > maxLon)
                    ||((node.getEle() < (elevationProgress*100)) && (node.getType().compareTo("peak")==0)))
                continue;
            switch (node.getType()) {
                case "peak":
                    paint = peakPaint;
                    break;
                case "city":
                    paint = cityNamePaint;
                    break;
                case "water":
                    paint = waterNamePaint;
                    break;
                case "":
                    paint = compassPaint;
                    break;
            }
            compassPath.reset();
            compassPath.moveTo((height * (node.getLon() - minLon) / (maxLon - minLon)) - 100,
                    (width * (maxLat - node.getLat()) / (maxLat - minLat))-5);
            compassPath.lineTo((height * (node.getLon() - minLon) / (maxLon - minLon)) + 100,
                    (width * (maxLat - node.getLat()) / (maxLat - minLat))-5);

            if(node.getType().compareTo("peak")==0)
                canvas.drawTextOnPath(node.getName() + ", " +(int) node.getEle()+" m n.m.", compassPath, 0, 0, paint);
            else
                canvas.drawTextOnPath(node.getName(), compassPath, 0, 0, paint);
            canvas.drawCircle((height * (node.getLon() - minLon) / (maxLon - minLon)),
                    (width * (maxLat - node.getLat()) / (maxLat - minLat)),
                    3,paint);
        }
    }

    public void setLocation(Location location) {
        this.currentLocation = location;
        setMinMaxLatLon();
        invalidate();
    }
    private void setMinMaxLatLon() {
        // distance in kilometers for 1 degree of current latitude
        double kmPer1LonDegree = ((EARTH_RADIUS*Math.cos(Math.toRadians(currentLocation.getLatitude())))*2.0*Math.PI)/360.0;
        // distance in kilometers for 1 degree of longitude
        double kmPer1LatDegree=(EARTH_RADIUS*2.0*Math.PI)/360.0;

        this.minLon= (float) (currentLocation.getLongitude() -   ((MAX_DISTANCE / kmPer1LonDegree) / 2.0) * 0.1*distanceProgress);
        this.minLat= (float) (currentLocation.getLatitude()  -   ((MAX_DISTANCE / kmPer1LatDegree) / 2.0) * 0.1*distanceProgress);
        this.maxLon= (float) (currentLocation.getLongitude() +   ((MAX_DISTANCE / kmPer1LonDegree) / 2.0) * 0.1*distanceProgress);
        this.maxLat= (float) (currentLocation.getLatitude()  +   ((MAX_DISTANCE / kmPer1LatDegree) / 2.0) * 0.1*distanceProgress);
    }

    public void setNodes(LinkedList<Nodes> nodes) {
        this.nodes = nodes;
    }

    public void setWays(LinkedList<WaysNodes> ways) {
        this.ways = ways;
    }

    public void setDistanceProgress(int distanceProgress) {
        this.distanceProgress = distanceProgress;
        switch (distanceProgress / 4) {
            case 0:
                offset = 1;
                break;
            case 1:
                offset = 2;
                break;
            case 2:
                offset = 3;
                break;
            case 3:
                offset = 4;
                break;
            case 4:
                offset = 5;
                break;
            case 5:
                offset = 6;
                break;
            default:
                offset = 1;
                break;
        }
        setMinMaxLatLon();
        invalidate();
    }

    public void setElevationProgress(int elevationProgress) {
        this.elevationProgress = elevationProgress;
        invalidate();
    }

    public float getMinLat() {
        return minLat;
    }

    public float getMaxLat() {
        return maxLat;
    }

    public float getMinLon() {
        return minLon;
    }

    public float getMaxLon() {
        return maxLon;
    }

    public boolean isCity() {
        return city;
    }

    public void setCity(boolean city) {this.city = city;}

    public boolean isTourist() {
        return tourist;
    }

    public void setTourist(boolean tourist) {
        this.tourist = tourist;
    }

    public boolean isWater() {
        return water;
    }

    public void setWater(boolean water) {
        this.water = water;
    }

    public boolean isPeak() {
        return peak;
    }

    public void setPeak(boolean peak) {
        this.peak = peak;
    }
}
