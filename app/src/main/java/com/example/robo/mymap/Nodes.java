package com.example.robo.mymap;

/**
 * Created by Robert on 20.6.2016.
 */
public class Nodes
{
    private float _lat;
    private float _lon;
    private String _name;
    private double _ele;
    private String _type;

    public String toString()
    {
        return _lat+","+_lon+","+_name+","+_ele+","+_type;

    }
    public float getLat() {
        return _lat;
    }

    public void setLat(float lat) {
        this._lat = lat;
    }

    public float getLon() {
        return _lon;
    }

    public void setLon(float lon) {
        this._lon = lon;
    }

    public String getName() {
        return _name;
    }

    public void setName(String name) {
        this._name = name;
    }

    public double getEle() {
        return _ele;
    }

    public void setEle(double ele) {
        this._ele = ele;
    }

    public String getType() {
        return _type;
    }

    public void setType(String type) {
        this._type = type;
    }
}
