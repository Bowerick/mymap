package com.example.robo.mymap;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Toast;

import java.util.LinkedList;
import java.util.List;

public class MainActivity extends Activity {

    private DBHelper myDB;
    private MyView myView;
    private LinearLayout mapLayout;
    private ImageButton peakButton;
    private ImageButton cityButton;
    private ImageButton touristButton;
    private ImageButton waterButton;
    private ImageButton modeButton;

    private boolean peak;
    private boolean city;
    private boolean tourist;
    private boolean water;
    private boolean manual;

    private SeekBar seekBarDistance;
    private SeekBar seekBarElevation;

    private EditText elevationEditText;
    private EditText distanceEditText;
    private EditText locationEditText;
    private static final long TWO_HOURS = 1000 * 60 * 60 * 2l;
    private static final float MIN_LAST_READ_ACCURACY = 500.0f;
    public static final String PREFS_NAME = "MyPrefsFile";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";

    private static final float MIN_DISTANCE = 20.0f;
    private static final long POLLING_FREQ = 1000 * 20;

    private LocationManager mLocationManager;
    private LocationListener mLocationListener;
    private Location mBestReading;
    private Location mBestReadingBeforeTouch;
    private int distanceProgress;
    private int elevationProgress;
    private boolean zoom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();

        mapLayout.addView(myView);
        myView.invalidate();

        //test
        //listeners
        peakButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(peak) {
                    peak = false;
                    peakButton.setImageResource(R.mipmap.peak_transparent);
                }else {
                    peak = true;
                    peakButton.setImageResource(R.mipmap.peak);
                }
                myView.setPeak(peak);
                myView.invalidate();
            }
        });

        cityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(city) {
                    city = false;
                    cityButton.setImageResource(R.mipmap.city_transparent);
                }else {
                    city = true;
                    cityButton.setImageResource(R.mipmap.city);
                }
                myView.setCity(city);
                myView.invalidate();
            }
        });

        touristButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(tourist) {
                    tourist = false;
                    touristButton.setImageResource(R.mipmap.tourist_transparent);
                }else {
                    tourist = true;
                    touristButton.setImageResource(R.mipmap.tourist);
                }
                myView.setTourist(tourist);
                myView.invalidate();
            }
        });

        waterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(water) {
                    water = false;
                    waterButton.setImageResource(R.mipmap.water_transparent);
                }else {
                    water = true;
                    waterButton.setImageResource(R.mipmap.water);
                }
                myView.setWater(water);
                myView.invalidate();
            }
        });

        modeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(manual) {
                    Log.i("MainActivity111",mBestReading.toString()+", "+mBestReadingBeforeTouch.toString());
                    mBestReading.setLongitude(mBestReadingBeforeTouch.getLongitude());
                    mBestReading.setLatitude(mBestReadingBeforeTouch.getLatitude());
                    myView.setLocation(mBestReadingBeforeTouch);
                    myView.invalidate();
                    new selectNodes().execute(myDB);
                    new selectWays().execute(myDB);

                    manual = false;
                    modeButton.setImageResource(R.mipmap.gps);
                }else {
                    manual = true;
                    modeButton.setImageResource(R.mipmap.hand);
                }
            }
        });

        mLocationListener = new LocationListener() {
            // Called back when location changes
            public void onLocationChanged(Location location) {
                if(!manual){
                    // Update best estimate
                    mBestReading = location;
                    mBestReadingBeforeTouch.setLatitude(mBestReading.getLatitude());
                    mBestReadingBeforeTouch.setLongitude(mBestReading.getLongitude());
                    Log.i("MainActivity222", mBestReading.toString() + ", " + mBestReadingBeforeTouch.toString());
                    //load data with AsyncTask
                    new selectNodes().execute(myDB);
                    new selectWays().execute(myDB);
                    myView.setLocation(mBestReading);
                    // Update display

                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.Toast) + "", Toast.LENGTH_LONG).show();
                    locationEditText.setText(getResources().getString(R.string.Poloha) + ": N" + (int) mBestReading.getLatitude() + (char) 0x00B0 + (int) ((mBestReading.getLatitude() % (int) mBestReading.getLatitude()) * 60) + "'" + (Math.round(((((mBestReading.getLatitude() % (int) mBestReading.getLatitude()) * 60) % (int) ((mBestReading.getLatitude() % (int) mBestReading.getLatitude()) * 60))) * 60)) + "'' "
                            + "E" + (int) mBestReading.getLongitude() + (char) 0x00B0 + (int) ((mBestReading.getLongitude() % (int) mBestReading.getLongitude()) * 60) + "'" + (Math.round(((((mBestReading.getLongitude() % (int) mBestReading.getLongitude()) * 60) % (int) ((mBestReading.getLongitude() % (int) mBestReading.getLongitude()) * 60))) * 60)) + "''");
                }
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
                // NA
            }

            public void onProviderEnabled(String provider) {
                // NA
            }

            public void onProviderDisabled(String provider) {
                // NA
            }
        };

        seekBarElevation.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                elevationProgress = progresValue;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                elevationEditText.setText(getResources().getString(R.string.MinVyska)+": " + ((elevationProgress + 1) * 100) + " m");
                myView.setElevationProgress(elevationProgress + 1);
            }
        });

        //set max distance
        seekBarDistance.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                distanceProgress = progresValue;
                Log.i("SeekBarDistance", "Value: " + progresValue);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                distanceEditText.setText(getResources().getString(R.string.MaxVzd)+": " + ((distanceProgress + 1) * 0.5) + " km");
                myView.setDistanceProgress(distanceProgress + 1);
            }
        });

        myView.setOnTouchListener(new View.OnTouchListener() {
            float start0X, start0Y, start1X, start1Y, distance, x, y;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Log.i("MainActivity333",mBestReading.toString()+", "+mBestReadingBeforeTouch.toString());
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_DOWN:
                            zoom = true;
                            start0X = event.getX();
                            start0Y = event.getY();
                            break;
                        case MotionEvent.ACTION_UP:
                            zoom = false;
                            new selectWays().execute(myDB);
                            break;
                        case MotionEvent.ACTION_POINTER_DOWN:
                            start1X = event.getX(1);
                            start1Y = event.getY(1);
                            zoom = true;
                            break;
                        case MotionEvent.ACTION_POINTER_UP:
                            zoom = false;
                            break;
                        case MotionEvent.ACTION_MOVE:
                            if (zoom) {
                                Log.i("MainActivity",event.getX()+", "+event.getY());
                                if (event.getPointerCount() < 2) {
                                    if(event.getX() <= myView.getWidth() && event.getX() >= myView.getWidth()/2.0 &&
                                            event.getY() <= myView.getHeight() && event.getY() >= myView.getHeight()-30){
                                        Log.i("MainActivity","link");
                                        Intent intent = new Intent();
                                        intent.setAction(Intent.ACTION_VIEW);
                                        intent.addCategory(Intent.CATEGORY_BROWSABLE);
                                        intent.setData(Uri.parse("http://www.openstreetmap.org"));
                                        startActivity(intent);
                                    }
                                    if(manual) {
                                        if (event.getX() > start0X) {
                                            mBestReading.setLongitude(mBestReading.getLongitude() - ((event.getX() - start0X) / (myView.getHeight())) * (myView.getMaxLon() - myView.getMinLon()));
                                        }
                                        if (event.getX() < start0X) {
                                            mBestReading.setLongitude(mBestReading.getLongitude() + ((start0X - event.getX()) / (myView.getHeight())) * (myView.getMaxLon() - myView.getMinLon()));
                                        }
                                        if (event.getY() > start0Y) {
                                            mBestReading.setLatitude(mBestReading.getLatitude() + ((event.getY() - start0Y) / (myView.getWidth())) * (myView.getMaxLat() - myView.getMinLat()));
                                        }
                                        if (event.getY() < start0Y) {
                                            mBestReading.setLatitude(mBestReading.getLatitude() - ((start0Y - event.getY()) / (myView.getWidth())) * (myView.getMaxLat() - myView.getMinLat()));
                                        }
                                        locationEditText.setText("Poloha: N" + (int) mBestReading.getLatitude() + "°" + (int) ((mBestReading.getLatitude() % (int) mBestReading.getLatitude()) * 60) + "'" + (Math.round(((((mBestReading.getLatitude() % (int) mBestReading.getLatitude()) * 60) % (int) ((mBestReading.getLatitude() % (int) mBestReading.getLatitude()) * 60))) * 60)) + "'' "
                                                + "E" + (int) mBestReading.getLongitude() + "°" + (int) ((mBestReading.getLongitude() % (int) mBestReading.getLongitude()) * 60) + "'" + (Math.round(((((mBestReading.getLongitude() % (int) mBestReading.getLongitude()) * 60) % (int) ((mBestReading.getLongitude() % (int) mBestReading.getLongitude()) * 60))) * 60)) + "''");
                                        new selectNodes().execute(myDB);
                                        myView.setLocation(mBestReading);
                                        myView.invalidate();
                                        start0X = event.getX();
                                        start0Y = event.getY();
                                    }
                                } else {
                                    int shift = 0;
                                    x = Math.abs(start0X - start1X);
                                    y = Math.abs(start0Y - start1Y);
                                    distance = (float) Math.sqrt(x * x + y * y);

                                    x = Math.abs(event.getX() - event.getX(1));
                                    y = Math.abs(event.getY() - event.getY(1));

                                    if (distance < Math.sqrt(x * x + y * y)) {
                                        shift = (int) (((-1) * Math.sqrt(x * x + y * y)) / distance);
                                    } else {
                                        shift = (int) (distance / Math.sqrt(x * x + y * y));
                                    }
                                    distanceProgress = distanceProgress + shift;
                                    if (distanceProgress < 0)
                                        distanceProgress = 0;
                                    if (distanceProgress > 9)
                                        distanceProgress = 9;
                                    seekBarDistance.setProgress(distanceProgress);
                                    distanceEditText.setText("Max. vzdialenosť: " + ((distanceProgress + 1) * 0.5) + " km");
                                    myView.setDistanceProgress(distanceProgress + 1);
                                    start0X = event.getX();
                                    start0Y = event.getY();
                                    start1X = event.getX(1);
                                    start1Y = event.getY(1);
                                }
                            }
                            break;
                    }
                return true;
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, POLLING_FREQ, MIN_DISTANCE, mLocationListener);
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, POLLING_FREQ, MIN_DISTANCE, mLocationListener);
            mLocationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, POLLING_FREQ, MIN_DISTANCE, mLocationListener);
        } catch (Exception e) {
            Log.e("MainActivity",""+e.toString());
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
            SharedPreferences.Editor edit = settings.edit();
            edit.putFloat(LATITUDE, (float) mBestReading.getLatitude());
            edit.putFloat(LONGITUDE, (float) mBestReading.getLongitude());
            edit.commit();
        }catch(Exception ex) {
            Log.i("MainActivity",""+ex.toString());
        }
    }

    private void init() {
        myDB = new DBHelper(this);
        myView = new MyView(this);

        mapLayout = (LinearLayout) findViewById(R.id.map);
        peakButton = (ImageButton) findViewById(R.id.buttonPeak);
        peak = true;
        cityButton = (ImageButton) findViewById(R.id.buttonCity);
        city = true;
        touristButton = (ImageButton) findViewById(R.id.buttonRoute);
        tourist = true;
        waterButton = (ImageButton) findViewById(R.id.buttonWater);
        water = true;
        modeButton = (ImageButton) findViewById(R.id.buttonMode);
        manual = false;

        elevationEditText = (EditText) findViewById(R.id.editTextElevation);
        distanceEditText = (EditText) findViewById(R.id.editTextDistance);
        locationEditText = (EditText) findViewById(R.id.editTextLocation);

        seekBarElevation = (SeekBar) findViewById(R.id.seekBar2);
        seekBarDistance = (SeekBar) findViewById(R.id.seekBar1);

        //set best last known location
        //if there is no loc... -> set Gerlach...   49.1639,20.1339
        mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        mBestReading = bestLastKnownLocation(MIN_LAST_READ_ACCURACY, TWO_HOURS);
        if (mBestReading != null) {
            myView.setLocation(mBestReading);
        } else {
            mBestReading = new Location("MyProvider");
            mBestReading.setLatitude(49.1639);
            mBestReading.setLongitude(20.1339);
            myView.setLocation(mBestReading);
        }

        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        mBestReading.setLatitude(settings.getFloat(LATITUDE,(float)mBestReading.getLatitude()));
        mBestReading.setLongitude(settings.getFloat(LONGITUDE,(float)mBestReading.getLongitude()));

        mBestReadingBeforeTouch=mBestReading;
        Log.i("MainActivity444",mBestReading.toString()+", "+mBestReadingBeforeTouch.toString());

        //distance progress 4 = 2.5km
        distanceProgress=4;
        myView.setDistanceProgress(distanceProgress+1);
        distanceEditText.setText(getResources().getString(R.string.MaxVzd)+": " + ((distanceProgress + 1) * 0.5) + " km");

        myView.setElevationProgress(0);
        elevationEditText.setText(getResources().getString(R.string.MinVyska)+": " + ((elevationProgress + 1) * 100) + " m");


        //load data with AsyncTask
        new selectNodes().execute(myDB);
        new selectWays().execute(myDB);
    }

    private Location bestLastKnownLocation(float minAccuracy, long maxAge) {
        Location bestResult = null;
        float bestAccuracy = Float.MAX_VALUE;
        long bestAge = Long.MIN_VALUE;

        List<String> matchingProviders = mLocationManager.getAllProviders();

        for (String provider : matchingProviders) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return null;
            }
            Location location = mLocationManager.getLastKnownLocation(provider);
            if (location != null) {
                float accuracy = location.getAccuracy();
                long time = location.getTime();
                if (accuracy < bestAccuracy) {
                    bestResult = location;
                    bestAccuracy = accuracy;
                    bestAge = time;
                }
            }
        }
        // Return best reading or null
        if (bestAccuracy > minAccuracy || (System.currentTimeMillis() - bestAge) > maxAge) {
            return null;
        } else {
            return bestResult;
        }
    }


    private class selectNodes extends AsyncTask<DBHelper, String, LinkedList<Nodes>> {
        @Override
        protected LinkedList<Nodes> doInBackground(DBHelper... myDB) {
            //publishProgress("Selecting nodes ...");
            return myDB[0].getNodes(mBestReading.getLatitude(), mBestReading.getLongitude());
        }

        @Override
        protected void onProgressUpdate(String... progress) {
        }

        @Override
        protected void onPostExecute(LinkedList<Nodes> result) {
            myView.setNodes(result);
            myView.invalidate();
        }
    }

    private class selectWays extends AsyncTask<DBHelper, String, LinkedList<WaysNodes>> {
        @Override
        protected LinkedList<WaysNodes> doInBackground(DBHelper... myDB) {
            //publishProgress("Selecting ways ...");
            return myDB[0].getWaysNodes(mBestReading.getLatitude(), mBestReading.getLongitude(), 20);
        }

        @Override
        protected void onProgressUpdate(String... progress) {
        }

        @Override
        protected void onPostExecute(LinkedList<WaysNodes> result) {
            myView.setWays(result);
            myView.invalidate();
        }
    }


}
